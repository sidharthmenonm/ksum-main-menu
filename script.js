
var menu_button = document.querySelector('.menu-button');
var hamburger = menu_button.children;
var main_menu = document.querySelector('.main-menu');
var side_bar = main_menu.querySelector('.sidebar-menu');
var slideout = main_menu.querySelector('.slideout');
var close_slideout = slideout.querySelector('.close');
var body = document.querySelector('body');

var open = false;
var current_open = '';

menu_button.addEventListener('click', function(){
  open = !open;

  if(open){
    //menu open
    main_menu.classList.remove('hidden');
    body.classList.add('overflow-hidden');

    hamburger[0].classList.remove('-translate-y-1.5');
    hamburger[0].classList.add('rotate-45');

    hamburger[1].classList.add('opacity-0');

    hamburger[2].classList.remove('translate-y-1.5');
    hamburger[2].classList.add('-rotate-45');


  }
  else{
    //menu close
    main_menu.classList.add('hidden');
    body.classList.remove('overflow-hidden');

    hamburger[0].classList.add('-translate-y-1.5');
    hamburger[0].classList.remove('rotate-45');

    hamburger[1].classList.remove('opacity-0');

    hamburger[2].classList.add('translate-y-1.5');
    hamburger[2].classList.remove('-rotate-45');

    main_menu.querySelector('[data-menu='+current_open+']').classList.add('-translate-x-full');
    slideout.classList.add('-translate-x-full');
    current_open = '';
    
  }

});

var menu_links = [].slice.call(document.querySelectorAll('[data-menu-target]'));

menu_links.map(function(link){
  console.log(link);

  link.addEventListener("mouseover", function(){
    slideout.classList.remove('-translate-x-full');

    if(current_open){
      main_menu.querySelector('[data-menu='+current_open+']').classList.add('-translate-x-full');
    }

    var target = link.getAttribute('data-menu-target');
    
    if(target){
      var menu = main_menu.querySelector('[data-menu='+target+']');
      menu.classList.remove('-translate-x-full')
    }
    else{
      slideout.classList.add('-translate-x-full');
    }

    current_open = target;
  });

})

close_slideout.addEventListener('click', function(){
  slideout.classList.add('-translate-x-full');
})